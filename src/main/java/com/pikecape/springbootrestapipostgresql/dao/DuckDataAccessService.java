package com.pikecape.springbootrestapipostgresql.dao;

import com.pikecape.springbootrestapipostgresql.model.Duck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("postgres")
public class DuckDataAccessService implements DuckDao
{
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DuckDataAccessService(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Duck> getAllDucks() {
        String sql = "SELECT id, name FROM duck";
        return jdbcTemplate.query(sql, (resultSet, i) -> {
            UUID id = UUID.fromString(resultSet.getString("id"));
            String name = resultSet.getString("name");
            return new Duck(id, name);
        });
    }


    @Override
    public Duck getDuck(UUID id)
    {
        String sql = "SELECT id, name FROM duck WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, ((resultSet, i) -> {
            UUID duckId = UUID.fromString(resultSet.getString("id"));
            String name = resultSet.getString("name");
            return new Duck(duckId, name);
        }));
    }

    @Override
    public Duck createDuck(Duck duck)
    {
        UUID id = UUID.randomUUID();
        String sql = "INSERT INTO duck (id, name) values (?, ?)";
        if (jdbcTemplate.update(sql, new Object[]{id, duck.getName()}) == 1) {
            return this.getDuck(id);
        } else {
            return null;
        }
    }

    @Override
    public Duck updateDuck(UUID id, Duck duck)
    {
        String sql = "UPDATE duck SET name = ? WHERE id = ?";
        if (jdbcTemplate.update(sql, new Object[]{duck.getName(), id}) == 1) {
            return this.getDuck(id);
        } else {
            return null;
        }
    }

    @Override
    public boolean deleteDuck(UUID id)
    {
        String sql = "DELETE FROM duck WHERE id = ?";
        return jdbcTemplate.update(sql, new Object[]{id}) == 1;
    }
}