package com.pikecape.springbootrestapipostgresql.dao;

import com.pikecape.springbootrestapipostgresql.model.Duck;

import java.util.List;
import java.util.UUID;

public interface DuckDao
{
    public List<Duck> getAllDucks();

    public Duck getDuck(UUID id);

    public Duck createDuck(Duck duck);

    public Duck updateDuck(UUID id, Duck duck);

    boolean deleteDuck(UUID id);
}
