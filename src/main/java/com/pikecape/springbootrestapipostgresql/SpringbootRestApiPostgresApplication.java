package com.pikecape.springbootrestapipostgresql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRestApiPostgresApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootRestApiPostgresApplication.class, args);
	}

}
