package com.pikecape.springbootrestapipostgresql.service;

import com.pikecape.springbootrestapipostgresql.dao.DuckDao;
import com.pikecape.springbootrestapipostgresql.model.Duck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DuckService
{
    private final DuckDao duckDao;

    @Autowired
    public DuckService(@Qualifier("postgres") DuckDao duckDao)
    {
        this.duckDao = duckDao;
    }

    public List<Duck> getAllDucks()
    {
        return duckDao.getAllDucks();
    }

    public Duck getDuck(UUID id)
    {
        return duckDao.getDuck(id);
    }

    public Duck createDuck(Duck duck)
    {
        return duckDao.createDuck(duck);
    }

    public Duck updateDuck(UUID id, Duck duck)
    {
        return duckDao.updateDuck(id, duck);
    }

    public boolean deleteDuck(UUID id)
    {
        return duckDao.deleteDuck(id);
    }
}
