package com.pikecape.springbootrestapipostgresql.api;

import com.pikecape.springbootrestapipostgresql.model.Duck;
import com.pikecape.springbootrestapipostgresql.service.DuckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@RequestMapping("api/v1/ducks")
@RestController
public class DuckController
{
    private final DuckService duckService;

    @Autowired
    public DuckController(DuckService duckService)
    {
        this.duckService = duckService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Duck> getAllDucks()
    {
        return duckService.getAllDucks();
    }

    @GetMapping(path = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public Duck getDuck(@PathVariable("id") UUID id)
    {
        return duckService.getDuck(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createDuck(@Valid @NotNull @RequestBody Duck duck)
    {
        duckService.createDuck(duck);
    }

    @PutMapping(path = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updateDuck(@PathVariable("id") UUID id, @Valid @NotNull @RequestBody Duck duck)
    {
        duckService.updateDuck(id, duck);
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDuck(@PathVariable("id") UUID id)
    {
        duckService.deleteDuck(id);
    }
}